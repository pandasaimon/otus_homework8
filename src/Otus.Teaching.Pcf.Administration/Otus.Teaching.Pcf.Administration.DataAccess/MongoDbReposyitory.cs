﻿using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoDbReposyitory<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IMongoCollection<T> _dtoCollection;

        public MongoDbReposyitory(IMongoCollection<T> dtoCollection)
        {
            _dtoCollection = dtoCollection;
        }
        
        public async Task AddAsync(T entity)
        {
            await _dtoCollection.InsertOneAsync(entity);
        }


    

         public async Task DeleteAsync(T entity)
         {
               await _dtoCollection.FindOneAndDeleteAsync(x=>x.Id==entity.Id);
           /*  if (result == null)
               return Task.  Console.WriteLine("Документы не найдены");
             else
                 Console.WriteLine($"Удален документ: {result}");*/
         }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dtoCollection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
           return await _dtoCollection.Find(x => x.Id == id).FirstOrDefaultAsync();
        }

        public async Task<T> GetFirstWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        { 
            return await _dtoCollection.FindAsync(predicate).Result.FirstAsync();
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public async  Task<IEnumerable<T>> GetWhere(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        { 
            return await _dtoCollection.FindAsync(predicate).Result.ToListAsync();           
        }

        public async Task UpdateAsync(T entity)
        {
            await _dtoCollection.ReplaceOneAsync(x => x.Id == entity.Id, entity);          
        }
    }

}
